<?php
	if (isset($_POST["submit"])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$human = intval($_POST['human']);
		$from = 'Pump It Up Contact Form'; 
		$to = 'bonita@pumpitup.co.za'; 
		$subject = 'Message Pumpitup Form ';
		
		$body ="From: $name\n E-Mail: $email\n Message:\n $message";

		// Check if name has been entered
		if (!$_POST['name']) {
			$errName = 'Please enter your name';
		}
		
		// Check if email has been entered and is valid
		if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Please enter a valid email address';
		}
		
		//Check if message has been entered
		if (!$_POST['message']) {
			$errMessage = 'Please enter your message';
		}
		//Check if simple anti-bot test is correct
		if ($human !== 5) {
			$errHuman = 'Your anti-spam is incorrect';
		}

// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
	if (mail ($to, $subject, $body, $from)) {
		$result='<div class="alert alert-success">Thank You! I will be in touch</div>';
	} else {
		$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later.</div>';
	}
}
	}
?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		
		<title>Pumpitup Gym</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/templatemo-style.css">
		<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">
		<link rel="icon" type="image/png"  href="/somewhere/myicon.png" />
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.singlePageNav.min.js"></script>
		<script src="js/typed.js"></script>
		<script src="js/wow.min.js"></script>
		<script src="js/custom.js"></script>
		<script src="js/embed.js"></script>
		



	</head>
	<body id="top">

		<!-- start preloader -->
		<div class="preloader">
			<div class="sk-spinner sk-spinner-wave">
     	 		<div class="sk-rect1"></div>
       			<div class="sk-rect2"></div>
       			<div class="sk-rect3"></div>
      	 		<div class="sk-rect4"></div>
      			<div class="sk-rect5"></div>
     		</div>
    	</div>
    	<!-- end preloader -->

        <!-- start header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p><i class="fa fa-phone"></i><span> Phone</span>082 574 0753</p>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p><i class="fa fa-envelope-o"></i><span> Email</span><a href="http://pumpitup.co.za/">bonita@pumpitup.co.za</a></p>
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-12">
                        <ul class="social-icon">
                            <li><span>Meet us on</span></li>
                            <li><a href="https://www.facebook.com/Pump-It-Up-Gym-1594152174235455/" class="fa fa-facebook"></a></li>
                            <li><a href="http://www.instagram.com/pumpitup_gym" class="fa fa-instagram"></a></li>
                         </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

    	<!-- start navigation -->
		<nav class="navbar navbar-default templatemo-nav" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<div class "logo"><a href="#" class="navbar-brand"><img src="../images/Logo.png" width="200" height="100%"/a></a></div>
				
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#top">HOME</a></li>
						<li><a href="#about">ABOUT US</a></li>
                        <li><a href="#classes">MY STORY</a></li>						
						<li><a href="#portfolio">GALLERY</a></li>
						<li><a href="#client">WHAT OUR CLIENTS SAY</a></li>
						<li><a href="#contact">CONTACT</a></li>						
					</ul>
				</div>
			</div>
		</nav>
		<!-- end navigation -->

    	<!-- start home -->
    	<section id="home">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-offset-2 col-md-8">
    					<h1 class="wow fadeIn" data-wow-offset="50" data-wow-delay="0.9s">Our Mission Is To Transform Your Life <span>Results Guaranteed</span>
						</h1>
						<div class="element" style="font-size:30px;color:white;">
                            <div class="sub-element" >Break the habit and challenge yourself.</div>
                            <div class="sub-element">Pump It Up is a place for you to change your life,</div>
                            <div class="sub-element">become healthier, fitter and stronger.</div> 
                        </div>
    					
    					
    				</div>
    			</div>
    		</div>
			
    	</section>
    	<!-- end home -->

    	<!-- start about -->
		<section id="about" class="p">
			<div class="container" >
				<div class="row">
					<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">ABOUT US</h2>
    				</div>
			
					</div>
					
					
			<p align="left"><img src="../images/bonita.jpg" width="300" height="187" align="right">
				My name is Bonita Louw. I have been doing personal training since 2010. I have completed the Professional Fitness Diploma, the Personal Fitness Trainer Diploma as well as the Nutritional Body Diploma. 
				I am very passionate about what I do and my aim is to help men and woman feel better about them. I assist with setting goals, eating plans, measurements and monthly targets. 
				I have competed in the Rossi Classic and have trained clients to be competition and stage ready.<p>
				My training is based on the principle of muscle confusion. One must shock the body to change the body. No two workouts are the same because no two bodies are the same. People evolve, so should their workouts. My fitness philosophy is comprised of a unique combination of functional movements, resistance training and cardiovascular conditioning.<p> I like to create customized programs for each client that combines these disciplines to meet the individual goals of the client – it's all about creating YOUR best body, YOUR best life. The five pillars for success are: Form, Function, Fun, Fatigue and Fuel
				Form
				Teaching body awareness and how the body works is key to creating a safe and effective program. Form is paramount to results... without it, the work is not maximized.
				Function
				My workouts are carefully thought out with the end goal in mind. Every workout has a goal in mind. Every movement has a purpose.<p>
				Fun
				Yes, working out can be fun. It may not be the kind of fun you'd equate with vacationing in the South of France, but I put together workouts and class programmes that are always different, always interesting and leave you feeling fantastic.
				Fatigue
				The end goal of every workout. Fatigue builds strength which creates results both inside and out.
				Fuel
				Providing the body with proper nutrition and rest is imperative in order to achieve YOUR best body. I will help you find the nutritional formula that will bring your physique and health to the next level. The fuel you put into your body is as important as the effort you put forth during workouts. My ultimate goals are to: Inspire. Empower. Motivate. Get results.

					</p>
		
				</div>
			</div>
		</section>
		<!-- end about -->

    	<!-- start classes -->
    	
    	<!-- end classes -->

    	<!-- start service -->
    	<section id="classes" class="p">
			<div class="container" >
				<div class="row">
					<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">My Story</h2>
    				</div>
					
					<center>
					<div >
					<table  width="100%" >
					<tr>
			        <td><img src="../images/SKJ_3652.jpg" width="200" height="300" align="left" ></td>
			        <td><img src="../images/DSC_5366.jpg" width="200" height="300" align="left" ></td>
                    <td><img src="../images/DSC_5372.jpg" width="200" height="300" align="left" ></td>
					<td><img src="../images/DSC_5377.jpg" width="200" height="300" align="left" ></td>
					<td><img src="../images/SKJ_3604.jpg" width="200" height="300" align="left" ></td>
				
					
					</tr>
					</table>
					
					</div>
					</center>
					
				</div>
					

					
			<hr>
			    <p><h4>Do you ever sit and wonder and say to yourself, I wish I could do….</h4></p>
				
				<p class="p"> Well that’s the thoughts I always had about gym and excising and eventually getting on stage.
				There has to be an end goal. Like anything in life you need to set your sight on something that you want and feel like that’s so tough to achieve. The harder something seems for you to achieve the stronger you get to eventually make that goal.
				I never used to train, I despised all forms of cardio, and I would always say to myself I wish I had a body like that or I wish I could train like that, I wish, I wish ….. until one day I thought about actually just giving it a go and setting my own personal goals.</p>
				
				<p  class="p">I started training in 2014 from my own home gym, I did my personal training diploma and wanted to see how I could transform my own body. I tried using various trainers to assist me in pushing me to where I wanted to be but nobody gave me the WOW factor with training, It was always the standard straight forward training and I got bored, I decided to start training friends and saw amazing results by doing my own training programs and shaking things up … Results were amazing.
				This became my passion. Woman tend to shy away from big gyms as we are never always sure of what we are doing or what we need to do and thus the reason my training encourages woman to become better for themselves. Look better and eat healthier, to live a healthy Lifestyle.
				I got on stage for the first time this year and I was super nervous, the journey was incredible. I took 4 months to drop weight and to drop body fat.</p>
				
				<p class="p">The lead up to the competition is tough but if your mind is set in the right space and you set your goals and sight on what you want, anything is possible and only you can make it happen. And Yes I did, I am living proof that you can do it with discipline and hard work and consistency YOU CAN ACHIEVE ANYTHING.
				You eating has to be right and your mind set as strong as possible, with a good trainer and strong determination its worth doing.</p>
				
				<h4>Ladies never accept just the ordinary, go for the lime light and DO IT. I can help you. Contact me for a free assessment and lets get you started….</h4> 
				<h4>The journey is 100% worth it.</h4>


			
		
		     </div>
			
    	</section>
    	<!-- end servie -->

    	<!-- start portfolio -->
    	<section id="portfolio">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">GALLERY</h2>
    				</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.labnol, img.labnol {max-width:100%; width: 100% !important; height: auto !important;}</style> <video class='labnol' width='612' height='612' controls poster='https://instagram.fash1-1.fna.fbcdn.net/t51.2885-15/e15/21436137_148088652446648_9015949808224960512_n.jpg'><source src='https://instagram.fash1-1.fna.fbcdn.net/t50.2886-16/21537668_1928500200730913_5296349898998808576_n.mp4' type='video/mp4'></video>Credit: @<a href='https://instagram.com/pumpitup_gym'>Pump It Up Gym</a>
                        </div>
    				</div>					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.labnol, img.labnol {max-width:100%; width: 100% !important; height: auto !important;}</style> <video class='labnol' width='612' height='612' controls poster='https://instagram.fash1-1.fna.fbcdn.net/t51.2885-15/e15/21372087_1554851681240553_872139984725868544_n.jpg'><source src='https://instagram.fash1-1.fna.fbcdn.net/t50.2886-16/21440152_1431719793586598_8531125211133640704_n.mp4' type='video/mp4'></video>Credit: @<a href='https://instagram.com/pumpitup_gym'>Pump It Up Gym</a>
                        </div>
    				</div>					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.labnol, img.labnol {max-width:100%; width: 100% !important; height: auto !important;}</style> <video class='labnol' width='612' height='612' controls poster='https://instagram.fash1-1.fna.fbcdn.net/t51.2885-15/e15/21224152_164319147457291_4849035471801548800_n.jpg'><source src='https://instagram.fash1-1.fna.fbcdn.net/t50.2886-16/21275532_843038292520781_1149920168575500288_n.mp4' type='video/mp4'></video>Credit: @<a href='https://instagram.com/pumpitup_gym'>Pump It Up Gym</a>
                        </div>
    				</div>				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.labnol, img.labnol {max-width:100%; width: 100% !important; height: auto !important;}</style> <video class='labnol' width='612' height='612' controls poster='https://instagram.fash1-1.fna.fbcdn.net/t51.2885-15/e15/20837626_110749352970360_720153143798661120_n.jpg'><source src='https://instagram.fash1-1.fna.fbcdn.net/t50.2886-16/20903553_1490610594318959_1598142216851685376_n.mp4' type='video/mp4'></video>Credit: @<a href='https://instagram.com/pumpitup_gym'>Pump It Up Gym</a>
                        </div>
    				</div>				
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.labnol, img.labnol {max-width:100%; width: 100% !important; height: auto !important;}</style> <video class='labnol' width='612' height='612' controls poster='https://instagram.fash1-1.fna.fbcdn.net/t51.2885-15/e15/19954837_200476387149195_6972260118016557056_n.jpg'><source src='https://instagram.fash1-1.fna.fbcdn.net/t50.2886-16/20021173_754895438023802_6370217715864436736_n.mp4' type='video/mp4'></video>Credit: @<a href='https://instagram.com/pumpitup_gym'>Pump It Up Gym</a>
                        </div>
    				</div>					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.labnol, img.labnol {max-width:100%; width: 100% !important; height: auto !important;}</style> <video class='labnol' width='612' height='612' controls poster='https://instagram.fash1-1.fna.fbcdn.net/t51.2885-15/e15/20065721_324640714645924_37756134081167360_n.jpg'><source src='https://instagram.fash1-1.fna.fbcdn.net/t50.2886-16/20189033_1629825893703420_2318329969496817664_n.mp4' type='video/mp4'></video>Credit: @<a href='https://instagram.com/pumpitup_gym'>Pump It Up Gym</a>
                        </div>
    				</div>					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.labnol, img.labnol {max-width:100%; width: 100% !important; height: auto !important;}</style> <video class='labnol' width='612' height='612' controls poster='https://instagram.fash1-1.fna.fbcdn.net/t51.2885-15/e15/20590133_798515106975846_7168622689895055360_n.jpg'><source src='https://instagram.fash1-1.fna.fbcdn.net/t50.2886-16/20611546_163749870853337_197580113502535680_n.mp4' type='video/mp4'></video>Credit: @<a href='https://instagram.com/pumpitup_gym'>Pump It Up Gym</a>
                        </div>
    				</div>					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.labnol, img.labnol {max-width:100%; width: 100% !important; height: auto !important;}</style> <video class='labnol' width='612' height='612' controls poster='https://instagram.fash1-1.fna.fbcdn.net/t51.2885-15/e15/20759816_2024442834451807_1426262066661949440_n.jpg'><source src='https://instagram.fash1-1.fna.fbcdn.net/t50.2886-16/20730395_1877151082549844_688162479326363648_n.mp4' type='video/mp4'></video>Credit: @<a href='https://instagram.com/pumpitup_gym'>Pump It Up Gym</a>
                        </div>
    				</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.instagram, img.instagram {width: 100% !important; height: auto !important;}</style> <video class="instagram" width="612" height="612" controls poster="https://scontent.cdninstagram.com/t51.2885-15/e15/16908677_1878499219062072_7985755258934525952_n.jpg"><source src="http://scontent.cdninstagram.com/t50.2886-16/17103014_1287521937991917_8291322622763859968_n.mp4" type="video/mp4"></video><br />Workout Wednesday @pumpitup_gym<a href='https://www.instagram.com/pumpitup_gym'>pumpitup_gym</a>
                        </div>
    				</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.instagram, img.instagram {width: 100% !important; height: auto !important;}</style> <video class="instagram" width="612" height="612" controls poster="https://scontent.cdninstagram.com/t51.2885-15/e15/13737067_278889495836735_1815096167_n.jpg"><source src="http://scontent.cdninstagram.com/t50.2886-16/14048039_233624130367160_313811201_s.mp4" type="video/mp4"></video><br />#pumpitupgym #fitnesssouthafrica #weights by @<a href='https://www.instagram.com/pumpitup_gym'>pumpitup_gym</a>
                        </div>
    				</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.instagram, img.instagram {width: 100% !important; height: auto !important;}</style> <video class="instagram" width="612" height="612" controls poster="https://scontent.cdninstagram.com/t51.2885-15/e15/14288196_340527849621197_1439314863116517376_n.jpg"><source src="http://scontent.cdninstagram.com/t50.2886-16/14443622_343243199349543_2226029129440952320_s.mp4" type="video/mp4"></video><br />#september #gloveson #boxingchallenge #keeppunching by @<a href='https://www.instagram.com/pumpitup_gym'>pumpitup_gym</a>
                        </div>
    				</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.instagram, img.instagram {width: 100% !important; height: auto !important;}</style> <video class="instagram" width="612" height="612" controls poster="https://scontent.cdninstagram.com/t51.2885-15/e15/p640x640/15101774_1193367544078186_698287740172632064_n.jpg"><source src="http://scontent.cdninstagram.com/t50.2886-16/15177527_683645615145383_2315023042247393280_n.mp4" type="video/mp4"></video><br />#traininsane #shoulders #back #legs #arms #thighs by @<a href='https://www.instagram.com/pumpitup_gym'>pumpitup_gym</a>
                        </div>
    				</div>
					</br> </br></br>
					<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
    					   <style>video.instagram, img.instagram {width: 100% !important; height: auto !important;}</style> <video class="instagram" width="612" height="612" controls poster="https://scontent.cdninstagram.com/t51.2885-15/e15/13722123_700913896742907_1553646513_n.jpg"><source src="http://scontent.cdninstagram.com/t50.2886-16/13839613_1151754751552274_1306719654_s.mp4" type="video/mp4"></video><br />#fitnesssouthafrica #personaltrainerjhb #jumps #weights by @<a href='https://www.instagram.com/pumpitup_gym'>pumpitup_gym</a>
                        </div>
    				</div>
					
    				<div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <style>video.instagram, img.instagram {width: 100% !important; height: auto !important;}</style> <video class="instagram" width="612" height="612" controls poster="https://scontent.cdninstagram.com/t51.2885-15/e15/15802962_659031574269679_5450434668241354752_n.jpg"><source src="http://scontent.cdninstagram.com/t50.2886-16/15969776_961783697322343_9056198062719893504_n.mp4" type="video/mp4"></video><br />Tip Thursday @pumpitup_gym #traininsane #results #dedication by @<a href='https://www.instagram.com/pumpitup_gym'>pumpitup_gym</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                           <style>video.instagram, img.instagram {width: 100% !important; height: auto !important;}</style> <video class="instagram" width="612" height="612" controls poster="https://scontent.cdninstagram.com/t51.2885-15/e15/16122770_241966089581141_7474065667176529920_n.jpg"><source src="http://scontent.cdninstagram.com/t50.2886-16/16147549_379366699084564_161720396461113344_n.mp4" type="video/mp4"></video><br />Fit Friday @ #fitness #motivation #results #dedication by @<a href='https://www.instagram.com/pumpitup_gym'>pumpitup_gym</a>
                        </div>
                    </div>
       
                    <div class="col-md-3 col-sm-6 col-xs-12 wow fadeIn" data-wow-offset="50" data-wow-delay="0.6s">
                        <div class="portfolio-thumb">
                          <style>video.instagram, img.instagram {width: 100% !important; height: auto !important;}</style> <video class="instagram" width="612" height="612" controls poster="https://scontent.cdninstagram.com/t51.2885-15/e15/13643454_579064435612342_1342875002_n.jpg"><source src="http://scontent.cdninstagram.com/t50.2886-16/13638026_590699327756132_1239966290_s.mp4" type="video/mp4"></video><br />#johannesburg #fitness #motivation  #bicycle #abs by @<a href='https://www.instagram.com/pumpitup_gym'>pumpitup_gym</a>
                        </div>
                    </div>
                    
                    
                    
    			</div>


				
				
    		</div>
    	</section>
    	<!-- end portfolio -->
		
		
		<!-- START TESTIMONIAL -->
		
		<section id="client" class="p">
			<div class="container" >
				<div class="row">
					<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">WHAT OUR CLIENTS SAY</h2>
    				</div>
			
					</div>
					
					
			<p align="left">
			
			
				<p style="font-family:comic sans; font-size: 130%;"><b>Savanah Zoghby</b></p>
				<blockquote><p style="font-family:comic sans; font-size: 100%;"><i> "I have always been someone who enjoys being physically active, though have never been one to enjoy gym. In actual fact I hated gym. Bonita changed all that for me. Gym with Bonita is the one thing I look forward to most in my day. Bonita not only cares about your physical well being but cares for your emotional well being too. The classes are small and intimate so you don't only work out with a group of other people but it is a family that you become a part of. This family is accepting and thrives to push each other to reach our end goals. The classes are intensive and Bonita pushes you so that you can train to your full ability. The best results that I have seen in my physical training have been because of Bonita. Her classes are a lot of work but also allow you to constantly challenge yourself. In a short amount of time I had lost more weight and gained muscle better then in any other training that I have done. Bonitas classes make you feel better about yourself in every aspect. If I could forcefully make anyone do anything, it would be to take them to Bonita."</i></p></blockquote>

			</p>
		
				</div>
			</div>
		</section>
		
		<!-- END TESTIMONIAL -->
		

    	<!-- start contact -->
    	<section id="contact">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">CONTACT US</h2>
    				</div>
    				<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-offset="50" data-wow-delay="0.9s">
					
		
				<form class="form-horizontal" role="form" method="post" action="index.php">
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="<?php echo htmlspecialchars($_POST['name']); ?>">
							<?php echo "<p class='text-danger'>$errName</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<?php echo htmlspecialchars($_POST['email']); ?>">
							<?php echo "<p class='text-danger'>$errEmail</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="message" class="col-sm-2 control-label">Message</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="4" name="message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
							<?php echo "<p class='text-danger'>$errMessage</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
							<?php echo "<p class='text-danger'>$errHuman</p>";?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<input id="submit" name="submit" type="submit" value="Submit" class="btn btn-primary">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<?php echo $result; ?>	
						</div>
					</div>
		</form> 
			
						
    				</div>
    				<div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight" data-wow-offset="50" data-wow-delay="0.6s">
    					<address>
    						<p class="address-title">Bonita Louw</p>
    						<p><i class="fa fa-phone"></i>082 574 0753</p>
    						<p><i class="fa fa-envelope-o"></i>bonita@pumpitup.co.za</p>
    					</address>
    					<ul class="social-icon">
    						<li><h4>WE ARE SOCIAL</h4></li>
    						<li><a href="https://www.facebook.com/Pump-It-Up-Gym-1594152174235455/" class="fa fa-facebook"></a></li>
    						<li><a href="http://www.instagram.com/pumpitup_gym" class="fa fa-instagram"></a></li>
    					</ul>
    				</div>
    			</div>
    		</div>
    	</section>
    	<!-- end contact -->

        <!-- start copyright -->
        <footer id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">
                       	Copyright &copy; 2017 PUMPITUP GYM</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end copyright -->

	</body>
</html>