
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		
		<title>Pumpitup Gym</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="css/templatemo-style.css">
		<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">
		<link rel="icon" type="image/png"  href="/somewhere/myicon.png" />
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.singlePageNav.min.js"></script>
		<script src="js/typed.js"></script>
		<script src="js/wow.min.js"></script>
		<script src="js/custom.js"></script>
		<script src="js/embed.js"></script>
		



	</head>
	<body id="top">

		<!-- start preloader -->
		<div class="preloader">
			<div class="sk-spinner sk-spinner-wave">
     	 		<div class="sk-rect1"></div>
       			<div class="sk-rect2"></div>
       			<div class="sk-rect3"></div>
      	 		<div class="sk-rect4"></div>
      			<div class="sk-rect5"></div>
     		</div>
    	</div>
    	<!-- end preloader -->

        <!-- start header -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p><i class="fa fa-phone"></i><span> Phone</span>082 574 0753</p>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p><i class="fa fa-envelope-o"></i><span> Email</span><a href="http://pumpitup.co.za/">bonita@pumpitup.co.za</a></p>
                    </div>
                    <div class="col-md-5 col-sm-4 col-xs-12">
                        <ul class="social-icon">
                            <li><span>Meet us on</span></li>
                            <li><a href="https://www.facebook.com/Pump-It-Up-Gym-1594152174235455/" class="fa fa-facebook"></a></li>
                            <li><a href="http://www.instagram.com/pumpitup_gym" class="fa fa-instagram"></a></li>
                         </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

    	<!-- start navigation -->
		<nav class="navbar navbar-default templatemo-nav" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<div class "logo"><a href="#" class="navbar-brand"><img src="../images/Logo.png" width="200" height="100%"/a></a></div>
				
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#top">HOME</a></li>
						<li><a href="#about">ABOUT US</a></li>						
						<li><a href="#portfolio">GALLERY</a></li>
						<li><a href="#client">WHAT OUR CLIENTS SAY</a></li>
						<li><a href="#contact">CONTACT</a></li>
						<li><a href="#classes">MY STORY</a></li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- end navigation -->



    	<!-- start about -->
		<section id="classes" class="p">
			<div class="container" >
				<div class="row">
					<div class="col-md-12">
    					<h2 class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">ABOUT US</h2>
    				</div>
			
					</div>
					
					
			<p align="left"><img src="../images/bonita.jpg" width="300" height="187" align="right">
				My name is Bonita Louw. I have been doing personal training since 2010. I have completed the Professional Fitness Diploma, the Personal Fitness Trainer Diploma as well as the Nutritional Body Diploma. 
				I am very passionate about what I do and my aim is to help men and woman feel better about them. I assist with setting goals, eating plans, measurements and monthly targets. 
				I have competed in the Rossi Classic and have trained clients to be competition and stage ready.<p>
				My training is based on the principle of muscle confusion. One must shock the body to change the body. No two workouts are the same because no two bodies are the same. People evolve, so should their workouts. My fitness philosophy is comprised of a unique combination of functional movements, resistance training and cardiovascular conditioning.<p> I like to create customized programs for each client that combines these disciplines to meet the individual goals of the client – it's all about creating YOUR best body, YOUR best life. The five pillars for success are: Form, Function, Fun, Fatigue and Fuel
				Form
				Teaching body awareness and how the body works is key to creating a safe and effective program. Form is paramount to results... without it, the work is not maximized.
				Function
				My workouts are carefully thought out with the end goal in mind. Every workout has a goal in mind. Every movement has a purpose.<p>
				Fun
				Yes, working out can be fun. It may not be the kind of fun you'd equate with vacationing in the South of France, but I put together workouts and class programmes that are always different, always interesting and leave you feeling fantastic.
				Fatigue
				The end goal of every workout. Fatigue builds strength which creates results both inside and out.
				Fuel
				Providing the body with proper nutrition and rest is imperative in order to achieve YOUR best body. I will help you find the nutritional formula that will bring your physique and health to the next level. The fuel you put into your body is as important as the effort you put forth during workouts. My ultimate goals are to: Inspire. Empower. Motivate. Get results.

					</p>
		
				</div>
			</div>
		</section>
		<!-- end about -->



        <!-- start copyright -->
        <footer id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="wow bounceIn" data-wow-offset="50" data-wow-delay="0.3s">
                       	Copyright &copy; 2017 PUMPITUP GYM</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end copyright -->

	</body>
</html>