$(document).ready(function(){
    $('#search').blur(function() {
        if ('' == $('#search').val()) $('#search').val('Search');
    });
    $('#search').focus(function() {
        if ('Search' == $('#search').val()) $('#search').val('');
    });
    $('ul.genres li a').click( // category slider
        function() {
            var checkElement = $(this).next();
            if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('.genres li ul').slideUp(150);
                $(this).next().slideToggle(150);
            }
        }
    );
    $('ul.genres ul li a').click( // get stations by category
       function() {
            $.ajax({
                type: 'GET',
                url: 'index.php',
                data: 'action=get_genre_stations&id=' + $(this).parent().attr('id') + '&name=' + $(this).parent().attr('val'),
                success: function(data){
                    $('.stlist').fadeOut(400, function () {
                        $('.stlist').html(data);
                        $('.stlist').fadeIn(400);
                    });
                }
            });
        }
    );
});
function play(id) { // play function
   $('#rplayer').load('index.php?action=play&id=' + id, function() {});
    return false;
}
function get_stations_by_keyword() { // get stations by keyword
    var keyword = $('#search').val().replace(/ /g,"+");
    $.ajax({
        type: 'GET',
        url: 'index.php',
        data: 'action=get_keyword_stations&key=' + keyword,
        success: function(data){
            $('.stlist').fadeOut(400, function () {
                $('.stlist').html(data);
                $('.stlist').fadeIn(400);
            });
        }
    });
}
